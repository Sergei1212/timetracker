import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from './services/modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //title = 'timeTracker';
  public isShowModal: boolean;
  constructor(private router: Router, private modalService: ModalService) {}

   /*public openAccount(): void {
    this.router.navigate(['account']);
  }*/

  ngOnInit() {
    this.modalService.modalState.subscribe(state => {
      this.isShowModal = state;
    });
  }

 
 
  
}
