import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailCorrectValidator } from 'src/app/validators/email.validator';
import { defineLocale } from 'ngx-bootstrap/chronos';
import * as moment from 'moment';

import { ruLocale } from 'ngx-bootstrap/locale';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
defineLocale('ru', ruLocale); 


@Component({
    selector: 'account-page',
    templateUrl: './account-page.component.html',
    styleUrls: ['./account-page.component.scss'], 
    changeDetection: ChangeDetectionStrategy.OnPush//changeDetection-отловка изменений OnPush-не переотрисовывает страницу
    //исключения: @InPut(передача данных в компонент), Input(инпуты)
})

export class AccountPageComponent {

public test = 0;

    constructor(
        private authService: AuthService,
        private router: Router, 
        private activatedRoute: ActivatedRoute,
        private localService: BsLocaleService,
        private cdr: ChangeDetectorRef//переменная для принудительной перересовки страницы.
        ) {}

    public name: FormControl;
    public email: FormControl;
    public password: FormControl;
    public birth: FormControl;
    public userForm: FormGroup;

    public isShowPass: boolean;

    ngOnInit() {
        setInterval(() => {
            this.test++;
            if(this.test % 2 ===0) {
                //console.log(this.test);
                this.cdr.markForCheck();//markForCheck-метод для принудит.переотрисовки страницы.
            }
        }, 1000);
        this.activatedRoute.params.subscribe(param => {
            console.log(param['id']);//param.id
        });
        this.createFormFields();
        this.createUserForm();
        console.log(this.authService.getUserData());
        this.showUserData();
        this.localService.use('ru');//календарь руссификатор
    }

    private showUserData(): void {
        const userData = this.authService.getUserData();
        this.name.setValue(userData.name);
        this.email.setValue(userData.email);
        this.password.setValue(userData.password);
        const bsdate: Date = moment.unix(+userData.birth).toDate();
        this.birth.setValue(bsdate);
    }

    public createFormFields(): void {
        this.name = new FormControl('');
        this.email = new FormControl({value: '', disabled: true}, [emailCorrectValidator]);
        this.password = new FormControl('', [Validators.minLength(6), Validators.maxLength(15)]);
        this.birth = new FormControl('', )
    }

    private createUserForm(): void {
        this.userForm = new FormGroup({
            name: this.name,
            email: this.email,
            password: this.password,
            birth: this.birth
        })
    }

    public logout(): void{
        this.authService.logout();
        this.router.navigate(['home']);
    }
 
    public saveChanges(): void {
        //console.log(this.userForm.value);
        //this.email.setValue('ItClass@mail.ru');
        /*const bsdate = this.birth.value.toString().split('T')[0];
        const bsunix =  moment(bsdate, 'YYY-MM-DD').unix();*/
        const bsunix = moment(this.birth.value).unix();
        console.log(bsunix);
        this.birth.setValue(bsunix);
        console.log(this.userForm.getRawValue());//получить сырые значения
        this.authService.saveUserData(this.userForm.getRawValue());
        this.showUserData();
    }

    public showPass(): void {
        this.isShowPass = true;
    }
    public upPass(): void {
        this.isShowPass = false;
    }
   

}
