import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';//подключили Datepicker с сайта https://valor-software.com/ngx-bootstrap/#/datepicker

import { AccountPageComponent } from './account-page.component';

@NgModule({
    declarations: [
        AccountPageComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),//подключили Datepicker
        RouterModule.forChild([
            {
                path: '',
                component: AccountPageComponent//если мноо роутов для лен.загрузки
            }
        ])
    ]
})

export class AccountPageModule {}