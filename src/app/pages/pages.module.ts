import { NgModule } from "@angular/core";//
import { TrackTimeComponent } from '../components/track-time/track-time.component';
import { TrackListComponent } from '../components/track-list/track-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';//модуль с анимациями
import { 
    HomePageComponent, 
    //AccountPageComponent 
} from './';//
import { CommonModule } from "@angular/common";//
import { ClockPipe } from '../pipes/clock.pipe';//
import { TrackPipe } from "../pipes/track.pipe";
import { DatePipe } from '../pipes/date.pipe';





@NgModule({
    declarations: [
        HomePageComponent,
        //AccountPageComponent,// убрали в account-page.module.ts
        ClockPipe,
        TrackTimeComponent,
        TrackPipe,
        TrackListComponent,
        DatePipe
    ],
    exports: [
        HomePageComponent,
        //AccountPageComponent// убрали в account-page.module.ts
    ],
    imports: [
        CommonModule, 
        BrowserAnimationsModule
    ]
})
export class PagesModule {} 