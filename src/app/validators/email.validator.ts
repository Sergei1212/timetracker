import { FormControl } from "@angular/forms";
import { NULL_EXPR } from "@angular/compiler/src/output/output_ast";

export function emailCorrectValidator(control: FormControl) {
    const value = control.value;
    const emailRegex = /^([A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$)/gi;
    const result = emailRegex.test(value);
    if (result) {
        return null;
    } else {
        return {
            "emailValidator": {
                valid: false
            }
        }
    }
}