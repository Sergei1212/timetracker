import { Route } from '@angular/router';
import { HomePageComponent, /*AccountPageComponent - убрали для ленивой.загрузки*/ } from './pages';
import { AuthGuard } from './services/auth.guard';

export const routes: Route[] = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomePageComponent
    },
    {
        path: 'account/:id',
        //component: AccountPageComponent, //- убрали для ленивой.загрузки
        loadChildren: './pages/account/account-page.module#AccountPageModule',//для ленивой загрузки
        canActivate: [AuthGuard]
    }

];