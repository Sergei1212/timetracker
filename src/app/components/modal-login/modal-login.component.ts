import { Component, Output, EventEmitter } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { emailCorrectValidator } from '../../validators/email.validator';
import { AuthService } from 'src/app/services/auth.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
    selector: 'modal-login',
    templateUrl: './modal-login.component.html',
    styleUrls: ['./modal-login.component.scss']
})

export class ModalLoginComponent {

    public loginForm: FormGroup;
    public email: FormControl;
    public password: FormControl;
    
    constructor(private modalService: ModalService, private authService: AuthService, private router: Router) {}

    ngOnInit() {        
        this.createFormFieldes();
        this.createForm();
    }

    private createFormFieldes(): void {
        this.email = new FormControl('', [Validators.required, emailCorrectValidator]);
        this.password = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]);
    }

    private createForm(): void {
        this.loginForm = new FormGroup({
            email: this.email,
            password: this.password
        });
    }

    public closeModal(): void {
        this.modalService.closeModal();
    }

    public stopProp(event: MouseEvent): void {//??????????
        event.stopPropagation();
    }

    public sendData(): void {
        //console.log(this.loginForm.value);
        this.authService.doAuthorization({
            email: this.loginForm.value.email, 
            password: this.loginForm.value.password
        });
        this.router.navigate(['account', this.email.value]);//что бы в адресе был написан емаил ItClass@mail.ru
    }

}/////