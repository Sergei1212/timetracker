import { Component } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { TrackService } from 'src/app/services/track.service';
import * as moment from 'moment';

@Component({
    selector: 'track-time',
    templateUrl: './track-time.component.html',
    styleUrls: ['./track-time.component.scss'],
    animations: [
        trigger('trackerState', [
            state('normal', style({
                transform:'translateY(0)'
            })), 
            state('open', style({
                //transform: 'translateY(-400px)',
                height: '200px'
            })),
            transition('normal <=> open', animate(500)),
            //transition('open => normal', animate(500)),
        ])
    ]
})

export class TrackTimeComponent {
    
    public state: string = 'normal';
    public isStart = true;
    public isPaused = false;
    public startTime: number;
    public currentTime: number;

    private timer: any;

    constructor(private trackService: TrackService) {}

    ngOnInit() {
        this.loadStartTime();
        this.isStart = !this.trackService.isHaveOpenTrack();//для закрытия страницы.
        if (!this.isStart) {
            if (this.trackService.isPaused()) {
                this.isPaused = true;                
                this.startTime = this.trackService.getOpenTrack().start_time;
                this.trackService.resPause();                
                //this.startTime += this.trackService.getPauseSumm();
                this.startTime = this.trackService.getOpenTrack().id;
                this.currentTime = moment().unix();//
                this.trackService.pauseTrack();//
                //this.startTimer();
                //this.pauseTrack();                
            } else {
                this.startTime = this.trackService.getOpenTrack().start_time;
                this.startTimer();
            }
        }
    }

    public loadStartTime(): void {
        if (this.trackService.track && this.trackService.track.start_time && !this.trackService.track.stop_time) {
            this.startTime = this.trackService.track.start_time;
            this.startTimer();
        }
    }

    private startTimer(): void {
       this.currentTime = moment().unix();
       this.timer = setInterval(() => {
            this.currentTime = moment().unix();
        }, 1000);
    }

    /*public animationEnded(event): void {
        console.log('Animation ended ', event);
    }*/

    public get isOpen(): boolean {
        return this.state == 'open'
    }

    public toogleTrack(): void {
        this.state == 'normal' ? this.state = 'open' : this.state = 'normal';//? - это if()
    }

    public startTrack(): void {
        this.isStart = false;
        this.isPaused = false;
        this.trackService.startTrack();
        this.loadStartTime();
    }

    public stopTrack(): void {
        this.isStart = true;
        this.isPaused = false;
        this.trackService.stopTrack();
        clearInterval(this.timer);//останавливаем таймер с временем в методе startTimer()
        this.currentTime = 0;
    }

    public pauseTrack(): void {        
        this.isPaused = true;
        this.trackService.pauseTrack();
        clearInterval(this.timer);
    }    

    public resumeTrack(): void {        
        this.isPaused = false;
        this.trackService.resumeTrack();
        this.startTime += this.trackService.getPauseSumm();
        this.startTimer();
    }


    

    


}