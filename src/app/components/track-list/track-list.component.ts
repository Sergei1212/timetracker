import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { TrackService } from '../../services/track.service';
import { TrackItem } from 'src/app/models/time.model';

@Component ({
    selector: 'app-track-list',
    templateUrl: './track-list.component.html',
    styleUrls: ['./track-list.component.scss'],
    animations: [
        trigger('listState', [
            state('normal', style({
                height: 0
            })),
            state('open', style({
                height: '200px'
            })),
            transition('normal<=>open', animate(500))
        ])        
    ]//для анимации
})

export class TrackListComponent implements OnInit{
    
    public state = 'normal';
    public trackData: TrackItem[] = [];

    constructor(private trackService: TrackService) {}

    ngOnInit() {
        this.trackService.stopTrackEvent.subscribe(() => {
            this.loadTrackData();
        })
        this.loadTrackData();
        //console.log(this.trackData);
    }

    private loadTrackData(): void {
        this.trackData = this.trackService.getAllClosedTrack();
    }

    public toogleTrack(): void {       
        this.state === 'normal' ? this.state = 'open' : this.state = 'normal';
    }

    public get isOpen(): boolean {
        return this.state === 'open';
    }

    public removeItem(time: number): void {
        this.trackService.removeTrack(time);
    }



}