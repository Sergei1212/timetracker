import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'

@Pipe({
    name: 'trackPipe'
})

export class TrackPipe implements PipeTransform {
    transform(value: number) {
        if (value) {
            return moment.unix(value).utcOffset(0).format('HH:mm:ss');
        } else {
            return '00:00:00';
        }
        
    }
}
