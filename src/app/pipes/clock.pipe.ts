import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'

@Pipe({
    name: 'clock'
})

export class ClockPipe implements PipeTransform {
    transform(value: number, offset?: boolean) {
        if(offset) {
            return moment.unix(value).utcOffset(0).format('HH:mm');
        }
        return moment.unix(value).format('HH:mm');
        
    }
}
