import { Pipe, PipeTransform } from "@angular/core";
import * as moment from 'moment';


@Pipe({
    name: 'date'
})

export class DatePipe implements PipeTransform {
    transform(value: number) {
        return moment.unix(value).utcOffset(0).format('DD-MM-YYYY');
    }
}