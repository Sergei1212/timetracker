import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router'//

import { AppComponent } from './app.component';
import { PagesModule } from './pages/pages.module';//
import { routes } from './app-routes';//
import { TimeService } from './services/time-service';//
import { AuthGuard } from './services/auth.guard';//
import { ModalService } from './services/modal.service';
import { AuthService } from './services/auth.service';
import { TrackService } from './services/track.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ModalLoginComponent } from './components/modal-login/modal-login.component';//


@NgModule({
  declarations: [
    AppComponent,
    ModalLoginComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,//
    PagesModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }), //{ preloadingStrategy: PreloadAllModules } -ленивая загрузка в фоне пока залипает на одной странице
    FormsModule,
    ReactiveFormsModule   
  ],
  providers: [
    TimeService,
    AuthGuard,
    ModalService,
    AuthService,
    TrackService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
