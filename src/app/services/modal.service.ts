import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()

export class ModalService{

public modalState: Subject<boolean> = new Subject();

public showModal(): void {
    this.modalState.next(true);
}

public closeModal(): void {
    this.modalState.next(false);
}

}
