import { Injectable } from '@angular/core';
import { UserData } from '../models/user.modal';

@Injectable()

export class AuthService {
    constructor() {}
    
    doAuthorization(user: User): void {
        if (this.isUserValid(user)) {
            localStorage.setItem('isAuth', '1');
        } else {
            localStorage.setItem('isAuth', '0');
        }
    }

    private isUserValid(user: User): boolean {
        return (user.email == 'ItClass@mail.ru' && user.password == '123456')
    }

    isAuth(): boolean {
        const auth = localStorage.getItem('isAuth');
        if(+auth == 1) return true;
        return false;
        // return (+auth == 1);
    }

    logout(): void{
        localStorage.setItem('isAuth', '0');
    }

    saveUserData(userData: UserData):void {
        localStorage.setItem('userData', JSON.stringify(userData));
    }

    getUserData(): UserData {
        return JSON.parse(localStorage.getItem('userData'));
    }
    
}

interface User {
    email: string;
    password: string;
}
