import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { TrackItem } from '../models/time.model';
import { Subject } from 'rxjs';

@Injectable()

export class TrackService {

    public track: TrackItem;
    public stopTrackEvent: Subject<boolean> = new Subject();

    constructor() { }

    startTrack(): void {
        this.track = {
            id: 0,
            start_time: moment().unix(),
            pause_time: null,
            pause_summ: null,
            stop_time: null
        };
        const trackArray: TrackItem[] = JSON.parse(localStorage.getItem('trackArray'));
        if (trackArray && trackArray.length > 0) {
            trackArray.push(this.track);
            localStorage.setItem('trackArray', JSON.stringify(trackArray));
        } else {
            const newTrackArray: TrackItem[] = [];
            newTrackArray.push(this.track);
            localStorage.setItem('trackArray', JSON.stringify(newTrackArray));//JSON.stringify - преобразование в строку для локалсторедж(localStorag)        
        }
    }

    stopTrack(): void {
        const trackArray: TrackItem[] = JSON.parse(localStorage.getItem('trackArray'));
        //console.log(trackArray);
        const notClosedTrack: TrackItem = trackArray.find(track => track.stop_time === null);//перебераем массив объектов и ищем объект с пустым полем stop_time
        //console.log(notClosedTrack);        
        const index = trackArray.indexOf(notClosedTrack);
        notClosedTrack.stop_time = moment().unix();
        trackArray.splice(index, 1, notClosedTrack);//перезаписываем объекты. на место объекта с индексом index записываем notCloseTrack
        //console.log(trackArray);
        localStorage.setItem('trackArray', JSON.stringify(trackArray));//перезаписываем localStorage 
        this.stopTrackEvent.next(true);            
    }

    isHaveOpenTrack(): boolean {
        const notClosedTrack = this.getOpenTrack();
        if (notClosedTrack) { //return !!notClosedTrack//короткая запись
            return true;
        }
        return false;
    }

    getOpenTrack(): TrackItem {
        const trackArray: TrackItem[] = JSON.parse(localStorage.getItem('trackArray'));
        const notClosedTrack: TrackItem = trackArray.find(track => track.stop_time === null);
        return notClosedTrack;
    }

    private getAllTracks(): TrackItem[] {
        return JSON.parse(localStorage.getItem('trackArray'));
    }

    public getAllClosedTrack(): TrackItem[] {
        const allTracks = this.getAllTracks();
        return allTracks.filter(track => track.stop_time);
    }

    pauseTrack(): void {
        const trackArray = this.getAllTracks();
        const openTrack = this.getOpenTrack();
        const index = trackArray.indexOf(openTrack);
        openTrack.pause_time = moment().unix();
        trackArray.splice(index, 1, openTrack);
        localStorage.setItem('trackArray', JSON.stringify(trackArray));
    }

    resPause(): void {
        const currentTime: number = moment().unix();
        const trackArray = this.getAllTracks();
        const openTrack = this.getOpenTrack();
        const index = trackArray.indexOf(openTrack);
        openTrack.pause_summ = currentTime - openTrack.pause_time;
        openTrack.id = openTrack.start_time + openTrack.pause_summ;//
        trackArray.splice(index, 1, openTrack);
        localStorage.setItem('trackArray', JSON.stringify(trackArray));
    }

    resumeTrack(): void {
        const trackArray = this.getAllTracks();
        const openTrack = this.getOpenTrack();
        const currentime = moment().unix();
        const index = trackArray.indexOf(openTrack);
        openTrack.pause_summ = currentime - openTrack.pause_time;
        openTrack.pause_time = null;
        trackArray.splice(index, 1, openTrack);
        localStorage.setItem('trackArray', JSON.stringify(trackArray));

    }

    getPauseSumm(): number {
        return this.getOpenTrack().pause_summ;
    }

    isPaused(): boolean {
        const openTrack = this.getOpenTrack()
        if (openTrack) {
            return !(openTrack.pause_time === null);
        }
        return false;
    }

    removeTrack(startTime: number): void {
        const allTracks = this.getAllTracks();
        const trackItem = allTracks.find(track => track.start_time === startTime);
        const index = allTracks.indexOf(trackItem);
        allTracks.splice(index, 1);
        localStorage.setItem('trackArray', JSON.stringify(allTracks));
        this.stopTrackEvent.next();
    }


}